﻿<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Word International Ministries | History Makers</title>

	<!-- CSS -->
	<!-- Bootstrap core CSS -->
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">

	<!-- Owl Carousel -->
	<link href="assets/css/owl.theme.css" rel="stylesheet">
	<link href="assets/css/owl.carousel.css" rel="stylesheet">

	<!-- Magnific-popup lightbox -->
	<link href="assets/css/magnific-popup.css" rel="stylesheet">

	<!-- Simple text rotator -->
	<link href="assets/css/simpletextrotator.css" rel="stylesheet">

	<!-- Font Awesome CSS -->
	<link href="assets/css/font-awesome.min.css" rel="stylesheet" media="screen">

	<!-- Animate css -->
	<link href="assets/css/animate.css" rel="stylesheet">

	<!-- Custom styles CSS -->
	<link href="assets/css/style.css" rel="stylesheet" media="screen">
	<link href='http://fonts.googleapis.com/css?family=Clicker+Script' rel='stylesheet' type='text/css'>
	<style>

	</style>
</head>
<body>

	<div class="wrapper">

		<!-- Preloader -->

		<div id="preloader">
			<div id="status">
				<div class="status-mes"><h4>History Makers</h4></div>
			</div>
		</div>

		<!-- Navigation start -->

		<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
			<div class="container">

				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" id="brand-logo" href="#">
					<img class="img img-responsive" src="assets/images/hm2.png">
					</a>

				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="#intro">Home</a></li>
						<li><a href="#about">About</a></li>
						<li><a href="#wingen">WinGen</a></li>
						<li><a href="#contact">Contact</a></li>
						<!-- <li><a href="components.html">Components</a></li> -->
					</ul>
				</div>

			</div>
		</nav>

		<!-- Navigation end -->

		<!-- Intro section start -->

		<section id="intro" class="section">
			<!-- <img id="img-responsive" src="assets/images/wingen.jpg" alt=""> -->
			<div class="container" id="intro-container">
				<div class="row">

					<div class="col-md-12">
						<a href="#about">
							<div class="mouse-icon">
								<div class="wheel"></div>
							</div>
						</a>

					</div>

				</div><!-- .row -->

			</div><!-- .container -->

		</section>

		<!-- Intro section end -->

		<!-- Profile contact callout section -->

		<section id="profile-contact">

			<div class="container">

				<div class="row">

					<div class="col-sm-4 wow bounceInLeft">
						<div class="profile-item">
							<i class="fa fa-envelope-o"></i>
							<h5><a href="#">winhistorymakers@gmail.com</a></h5>
						</div>
					</div>

					<div class="col-sm-4 wow bounceInUp">
						<div class="profile-item">
							<i class="fa fa-phone"></i>
							<h5>09327242107</h5>
						</div>
					</div>

					<div class="col-sm-4 wow bounceInRight">
						<div class="profile-item">
							<i class="fa fa-map-marker"></i>
							<h5>8439 3rd flr. South Superhighway West Service Rd. Barangay Marcelo Green, Parañaque City</h5>
						</div>
					</div>

				</div><!-- .row -->

			</div><!-- .container -->

		</section>

		<!-- About contact callout section -->

		<!-- About section start -->

		<section id="about" class="section">

			<div class="container">

				<div class="row">

					<div class="col-md-12 headline wow bounceInDown">
						<h2>History Makers</h2>
						<p>The Youth of Word International Ministries - Sucat</p>
					</div>

					<div class="col-md-6 col-sm-12 wow bounceInRight">
						<h3>Mission and Vision</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam doloremque error fugiat quaerat iure sunt consectetur in deleniti saepe earum nesciunt quasi, rem dolores porro, officiis illo, corrupti consequatur quidem.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur alias quos est ea quidem culpa, facilis nisi, nam velit, nemo iste ad accusantium quisquam voluptate. Recusandae vitae alias et repellat?</p>
					</div>

					<div class="col-md-6 col-sm-12 wow bounceInLeft">
						<img src="assets/images/donotletanyone.jpg" alt="">
					</div>

				</div><!-- .row -->

			</div><!-- .container -->

		</section>

		<!-- About section end -->

		<section id="wingen" class="section">

			<div class="container">

				<div class="row">

					<div class="col-md-12 headline wow bounceInDown">
						<h2>Win Gen</h2>
						<p>Winning the next generation of youth.</p>
					</div>

					<div class="col-md-6 col-sm-12 wow flip">
						<h3>Mission</h3>
						<p>To reach the lives of the youth by sharing to them the word of God.</p>
						<p>“Therefore go and make disciples of all nations, baptizing them in the name of the Father and of the Son and of the Holy Spirit and teaching them to obey everything I have commanded you. And surely I am with you always, to the very end of the age.”</p>
						<h6>Matthew 28:19-20</h6>
					</div>

					<div class="col-md-6 col-sm-12 wow bounceInUp">
						<img src="assets/images/gointoalltheworld.jpg" alt="">
					</div>

				</div><!-- .row -->

			</div><!-- .container -->

		</section>


		<!-- Contact section start -->

		<section id="contact" class="section">

			<div class="container">

				<div class="row">

					<div class="col-md-12 headline wow bounceInLeft">
						<h2>Contact Us</h2>
						<p>Drop us a line or give us a ring.</p>
					</div>

					<div class="col-md-6 wow bounceInUp">

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis nostrum laborum maiores, doloribus aliquid repellat facilis, recusandae rerum autem quibusdam consectetur voluptatibus. Ea, beatae debitis, adipisci voluptatem dignissimos sequi odit.</p>

						<ul class="icon-list">
							<li>8439 3rd flr. South Superhighway West Service Rd. Barangay Marcelo Green, Parañaque City</li>
							<li><i class="fa fa-fw fa-phone"></i>0932724107</li>
							<li><i class="fa fa-fw fa-envelope-o"></i><a href="mailto:">winhistorymakers@gmail.com</a></li>
							<li><i class="fa fa-fw fa-globe"></i><a href="#">http://winhistorymakers.url.ph</a></li>
						</ul>

					</div>

					<div class="col-md-6 wow bounceInRight">
<?php include('mail.php'); ?>
						<form id="contact-form" role="form" method="post">

							<div class="form-group">
								<label class="sr-only" for="c_name">Name</label>
								<input type="text" id="c_name" class="form-control" name="c_name" placeholder="Name">
							</div>

							<div class="form-group">
								<label class="sr-only" for="c_email">Email address</label>
								<input type="email" id="c_email" class="form-control" name="c_email" placeholder="E-mail">
							</div>

							<div class="form-group">
								<textarea class="form-control" id="c_message" name="c_message" rows="7" placeholder="Your message"></textarea>
							</div>

							<input type="submit" value="Send it" class="btn btn-custom-1">

						</form>

						<div class="ajax-response"></div>

					</div>

				</div><!-- .row -->

			</div><!-- .container -->

		</section>

		<!-- Contact section end -->

		<!-- Footer start -->

		<footer id="footer">

			<div class="container">

				<div class="row">

					<div class="col-md-12">
						<p class="copy">
							© 2014 History Makers, All Rights Reserved.
						</p>
					</div>

				</div><!-- .row -->

			</div><!-- .container -->

		</footer>

		<!-- Footer end -->

	</div><!-- .wrapper -->

	<!-- Javascript files -->
	<!-- jQuery -->
	<script src="assets/js/jquery-1.11.0.min.js"></script>
	<!-- Bootstrap JS -->
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<!-- Background slider -->
	<script src="assets/js/jquery.backstretch.min.js"></script>
	<!-- OwlCarousel -->
	<script src="assets/js/owl.carousel.min.js"></script>
	<!-- Modal for portfolio -->
	<script src="assets/js/jquery.magnific-popup.min.js"></script>
	<!-- Text rotator -->
	<script src="assets/js/jquery.simple-text-rotator.min.js"></script>
	<!-- Waypoints -->
	<script src="assets/js/jquery.waypoints.js"></script>
	<!-- CountTo  -->
	<script src="assets/js/jquery.countTo.js"></script>
	<!-- WOW - Reveal Animations When You Scroll -->
	<script src="assets/js/wow.min.js"></script>   
	<!-- Smooth scroll -->
	<script src="assets/js/smoothscroll.js"></script>
	<!-- Custom scripts -->
	<script src="assets/js/custom.js"></script>

</body>
</html>