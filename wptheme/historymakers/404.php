<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div class="jumbotron">
    <h1 class="text-center">PAGE NOT FOUND</h1>
</div>
<?php
get_footer();