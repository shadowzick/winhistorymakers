<?php get_header(); ?>


       <?php $blog_args = array(
        'posts_per_page'   => 5,
        'category_name'    => 'blog',
        'orderby'          => 'post_date',
        'order'            => 'DESC',
        'post_status'      => 'publish'
    ); ?>


    <div class="container-fluid">
        <div class="row">
           <?php $blog_posts = get_posts( $blog_args );
            foreach ( $blog_posts as $post ) : setup_postdata( $post ); ?>
            <div class="col-md-6 col-xs-12">
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <p><?php the_excerpt() ;?></p>
            </div>
            <?php endforeach; ?>
        </div>
    </div>



<?php get_footer(); ?>