<?php get_header(); ?>


        <!-- Profile contact callout section -->
<?php $args = array(
    'posts_per_page'   => 3,
    'offset'           => 0,
    'category'         => '',
    'category_name'    => 'contact-info',
    'orderby'          => 'post_date',
    'order'            => 'DESC',
    'post_status'      => 'publish'
    ); ?>


 <?php if ( is_home() ) { ?>

        <section id="profile-contact">

            <div class="container">

                <div class="row">

                   <?php
                    $contact_info = get_posts( $args );
                    foreach ( $contact_info as $post ) : setup_postdata( $post ); ?>
                        <?php the_content(); ?>
                    <?php endforeach; 
                    wp_reset_postdata();?>

                </div><!-- .row -->

            </div><!-- .container -->

        </section>

        <!-- About contact callout section -->

        <!-- About section start -->


        <section id="about" class="section">

            <div class="container">

            <?php $post_args = array(
                'posts_per_page'   => 3,
                'offset'           => 0,
                'category'         => '',
                'category_name'    => 'on-index',
                'orderby'          => 'post_date',
                'order'            => 'DESC',
                'post_status'      => 'publish'
            ); ?>

                <div class="row">
                        
                            
                   <?php 

                   $posts = get_posts( $post_args );
                    foreach ( $posts as $post ) : setup_postdata( $post ); ?>


                    
                    <div class="col-md-12 headline wow bounceInDown">

                         <h2 id="post-<?php the_ID(); ?>">
                             <?php the_title(); ?>
                        </h2>
                        <!-- This subtitle is from the plugin wp-subtitle -->
                        <p><?php the_subtitle(); ?></p>

                    </div>
    
                    <?php the_content('Read the rest of this entry &raquo;'); ?>
                        
                    <div class="col-md-6 col-sm-12 wow bounceInLeft">
                   <?php
                    // Post thumbnails need this in functions.php: `add_theme_support('post-thumbnails');`
                        if(has_post_thumbnail()) the_post_thumbnail(array(700, 700));
                    ?>
                    </div>


                </div>
                        
                        <?php endforeach; 
                        wp_reset_postdata();?>

                        <!-- No posts found -->

        <!-- Contact section start -->

        <section id="contact" class="section">

            <div class="container">

                <div class="row">
                <?php $contact_args = array(
                    'posts_per_page'   => 1,
                    'offset'           => 0,
                    'category'         => '',
                    'category_name'    => 'contact-form',
                    'orderby'          => 'post_date',
                    'order'            => 'DESC',
                    'post_status'      => 'publish'
                ); ?>

               <?php
                $contact_form = get_posts( $contact_args );
                foreach ( $contact_form as $post ) : setup_postdata( $post ); ?>

                <?php the_content(); ?>

            <?php endforeach; ?>



                </div><!-- .row -->

            </div><!-- .container -->

        </section>

<?php } else { ?>

<div class="container-fluid">
    <div class="row">
    <?php if(have_posts()): while(have_posts()): the_post(); ?>
        
        <div <?php post_class(); ?>>
            
        <div class="col-md-6 col-md-offset-3 col-xs-12 col-lg-6 col-lg-offset-4">
            <?php
            //Post thumbnails need this in functions.php: `add_theme_support('post-thumbnails');`
            if(has_post_thumbnail()) the_post_thumbnail(array(500,500));
            ?>
        </div>
        <div class="col-md-12 col-xs-12">
            <h2 id="post-<?php the_ID(); ?>">
                <a href="<?php the_permalink(); ?>" title="Permanent link to <?php the_title(); ?>"><?php the_title(); ?></a>
            </h2>
            
            <div class="post-metadata">
                <p>Posted on <?php the_time('F jS, Y'); ?> by <?php the_author(); ?> in <?php the_category(', '); ?></p>
                <p><?php edit_post_link('Edit','','|'); ?> <?php comments_popup_link('No Comments &raquo;', '1 Comment &raquo;', '% Comments &raquo;'); ?></p>
            </div>
            
            <div class="post-content">
                <?php the_content('Read the rest of this entry &raquo;'); ?>
                <?php /* the_excerpt(); */ ?>
            </div>
            
            <div class="post-tags">
                <?php the_tags(); ?>
            </div>
        </div> 
        </div>
        
    <?php endwhile; ?>
        
        <div class="post-navigation">
            <div><?php posts_nav_link('', 'Next Entries &raquo;', ''); ?></div>
            <div><?php posts_nav_link('', '', '&laquo; Previous Entries'); ?></div>
        </div>
    </div>
</div>
    
<?php else: ?>
    <!-- No posts found -->
<?php endif; ?>
<?php } ?>


        <!-- Contact section end -->

        <!-- Footer start -->

            <?php get_footer(); ?>


        <!-- Footer end -->

    </div><!-- .wrapper -->

    
</body>
    <!-- Javascript files -->
    <!-- jQuery -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.11.0.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
    <!-- Background slider -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.backstretch.min.js"></script>
    <!-- OwlCarousel -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.min.js"></script>
    <!-- Modal for portfolio -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.magnific-popup.min.js"></script>
    <!-- Text rotator -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.simple-text-rotator.min.js"></script>
    <!-- Waypoints -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.waypoints.js"></script>
    <!-- CountTo  -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.countTo.js"></script>
    <!-- WOW - Reveal Animations When You Scroll -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/wow.min.js"></script>   
    <!-- Smooth scroll -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/smoothscroll.js"></script>
    <!-- Custom scripts -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>
</html>