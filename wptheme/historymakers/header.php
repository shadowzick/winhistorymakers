<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    
    <meta charset="<?php bloginfo('charset'); ?>" />
    
    <title><?php wp_title(); ?> <?php bloginfo('name'); ?></title>
    
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo get_stylesheet_uri(); ?>" />
    <!-- CSS -->
    <!-- Bootstrap core CSS -->
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet" media="screen">

    <!-- Owl Carousel -->
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/owl.theme.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/owl.carousel.css" rel="stylesheet">

    <!-- Magnific-popup lightbox -->
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/magnific-popup.css" rel="stylesheet">

    <!-- Simple text rotator -->
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/simpletextrotator.css" rel="stylesheet">

    <!-- Font Awesome CSS -->
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/font-awesome.min.css" rel="stylesheet" media="screen">

    <!-- Animate css -->
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/animate.css" rel="stylesheet">

    <!-- Custom styles CSS -->
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/style.css" rel="stylesheet" media="screen">
    <link href='http://fonts.googleapis.com/css?family=Clicker+Script' rel='stylesheet' type='text/css'>    


    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php if(is_singular() && get_option('thread_comments')) wp_enqueue_script('comment-reply'); ?>
    
    <?php wp_head(); ?>
    
</head>
<body>
   
    <div class="wrapper">

        <!-- Preloader -->

    <?php if( is_home() ) { ?>

        <div id="preloader">
            <div id="status">
                <div class="status-mes"><h4>History Makers</h4></div>
            </div>
        </div>
    <?php } ?>

        <!-- Navigation start -->

        <nav class="navbar navbar-color navbar-custom navbar-fixed-top" role="navigation">
            <div class="container">

                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                <?php $navbar_logo = array(
                    'posts_per_page'   => 1,
                    'category_name'    => 'navbar-logo',
                    'orderby'          => 'post_date',
                    'order'            => 'DESC',
                    'post_status'      => 'publish'
                ); ?>

                    <a class="navbar-brand" id="brand-logo" href="#">
                       <?php
                            $navbar_logo_args = get_posts( $navbar_logo );
                            foreach ( $navbar_logo_args as $post ) : setup_postdata( $post ); ?>

                           <?php
                               $post_thumbnail_id = get_post_thumbnail_id();
                               $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
                           ?>
                           
                            <img class="img img-responsive" src="<?php echo $post_thumbnail_url;?>">

                        <?php endforeach; ?>
                    </a>

                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->




                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <?php if ( is_home() ){  ?>

                        <?php $navbar_args = array(
                            'posts_per_page'   => 1,
                            'category_name'    => 'navbar-ul',
                            'orderby'          => 'post_date',
                            'order'            => 'DESC',
                            'post_status'      => 'publish'
                        ); ?>
                        
                       <?php
                            $navbar = get_posts( $navbar_args );
                            foreach ( $navbar as $post ) : setup_postdata( $post ); ?>

                            <?php the_content(); ?>

                        <?php endforeach; ?>


               <?php } else { ?>

                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="<?php echo get_site_url(); ?>">Home</a></li>
                        <li><a href="<?php echo get_site_url(); ?>/blog">Blog</a></li>
                        <!-- <li><a href="components.html">Components</a></li> -->
                    </ul>
                <?php } ?>
                </div>


            </div>
        </nav>

<?php if( is_home() ) { ?>

        <!-- Navigation end -->
        <!-- Intro section start -->
        <?php $header_args = array(
            'posts_per_page'   => 1,
            'offset'           => 0,
            'category'         => '',
            'category_name'    => 'header',
            'orderby'          => 'post_date',
            'order'            => 'DESC',
            'post_status'      => 'publish'
            ); ?>


            <div class="container-fluid" id="top">
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-lg-8 col-lg-offset-2">

               <?php
                $header = get_posts( $header_args );

                foreach ( $header as $post ) : setup_postdata( $post );
                   $thumbnail_id = get_post_thumbnail_id();
                   $thumbnail_url = wp_get_attachment_url( $thumbnail_id );
               ?>

                <img src="<?php echo $thumbnail_url; ?>" class="img-responsive">
                <br>

                <?php endforeach; ?>
                    </div>
                </div>
            </div>
<?php }else{?>
<br>
<br>
<?php } ?>



        <!-- Intro section end -->