        <footer id="footer">

            <div class="container">

                <div class="row">

                    <div class="col-md-12">
                        <p class="copy">
                            © 2014 History Makers, All Rights Reserved.
                        </p>
                    </div>

                </div><!-- .row -->

            </div><!-- .container -->

        </footer>

<?php if ( !is_home() ) { ?>

<style>
    footer#footer {
        margin-top: 50px;
    }
</style>

<?php } ?>